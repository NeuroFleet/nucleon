from nucleon.linguist.shortcuts import *

################################################################################

@Reactor.orm.register_model('dialect')
class Dialect(models.Model):
    person  = models.ForeignKey(Identity, related_name='dialects')

    alias   = models.CharField(max_length=24)
    title   = models.CharField(max_length=128, blank=True)

################################################################################

@Reactor.orm.register_model('word')
class Word(models.Model):
    dialect = models.ForeignKey(Dialect, related_name='words')
    literal = models.CharField(max_length=48)

#*******************************************************************************

@Reactor.orm.register_model('verb')
class Verb(models.Model):
    dialect = models.ForeignKey(Dialect, related_name='verbs')
    literal = models.CharField(max_length=48)

################################################################################

@Reactor.orm.register_model('topic')
class Topic(models.Model):
    dialect = models.ForeignKey(Dialect, related_name='topics')

    alias   = models.CharField(max_length=24)
    title   = models.CharField(max_length=128, blank=True)

    words   = models.ForeignKey(Word, related_name='words', blank=True)
    verbs   = models.ForeignKey(Verb, related_name='verbs', blank=True)

#*******************************************************************************

@Reactor.orm.register_model('expression')
class Expression(models.Model):
    dialect    = models.ForeignKey(Dialect, related_name='expressions')

    alias      = models.CharField(max_length=24)
    title      = models.CharField(max_length=128, blank=True)

    topics     = models.ForeignKey(Topic,  related_name='expressions', blank=True)

#*******************************************************************************

@Reactor.orm.register_model('lambda')
class ExprLambda(models.Model):
    expression = models.ForeignKey(Expression, related_name='lambdas')
    offset     = models.IntegerField(default=0)

    formula    = models.CharField(max_length=128, blank=True)
    role       = models.CharField(max_length=128, blank=True)
    literal    = models.CharField(max_length=128, blank=True)


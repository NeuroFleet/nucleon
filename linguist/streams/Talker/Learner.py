#-*- coding: utf-8 -*-

from reactor.shortcuts import *

from .psyverse import *

################################################################################

@Reactor.wamp.register_middleware('think.dreamer.Dyslexya')
class Markov(Nucleon):
    def on_open(self, details):
        pass

    ############################################################################

    def topics(self, details):
        yield u'broca.new_word', self.on_new_word

    #***************************************************************************

    def methods(self, details):
        yield u'intellect.dialect.answer',    self.answer
        yield u'intellect.dialect.smalltalk', self.smalltalk

    ############################################################################

    def on_new_word(self, msg):
        print("event received on {}: {}".format(topic, msg))

    ############################################################################

    def graph_word(self, stmt):
        pass


#-*- coding: utf-8 -*-

import re, humanize, uuid, simplejson

from django import template

from social_core.backends.oauth import OAuthAuth

from social_django.utils import Storage

from django.template import TemplateDoesNotExist, RequestContext, loader

#*******************************************************************************

register = template.Library()

NAME_RE = re.compile(r'([^O])Auth')

LEGACY_NAMES = ['username', 'email']

#*******************************************************************************

def get_icon(key):
    key = key.lower()

    mapping = {
        'console':      'dashboard',
        'search':       'search',
        'status':       'map-signs',
        # Layouts
        'blank':        'file-o',
        'gallery':      'th',
        'listing':      'table',
        'profile':      'vcard-o',
        'timeline':     'clock-o',
        'vectoriel':    'map',
        # Console
        'explorer':     'users',
        'hub':          'sitemap',
        'geo':          'globe',
        'graph':        'connectdevelop',
        'semantic':     'language',
        'records':      'database',
        'schemas':      'paw',
        'linked':       'link',
        # Status
        'contain':      'cubes',
        'monitor':      'signal',
        'storage':      'hdd-o',
        'stream':       'shuffle',
        # Search
        'biblio':       'book',
        'people':       'users',
        'organization': 'building',
        'governement':  'university',
        'web':          'globe',
        'media':        'bullhorn',
        #'media-text':           'file-text-o',
        #'media-word':           'file-word-o',
        #'media-code':           'file-code-o',
        #'media-image':          'file-image-o',
        #'media-audio':          'file-audio-o',
        #'media-video':          'file-video-o',

        'aol':                  'phone',
        'belgiumeid':           'id-card',
        'coinbase':             'bitcoin',
        'fedora':               'pied-piper',
        'livejournal':          'windows',
        'opensuse':             'hand-lizard-o',
        'persona':              'firefox',
        'pocket':               'get-pocket',
        'podio':                'podcast',
        'qiita':                'quora',
        'salesforce':           'money',
        'uber':                 'taxi',
        'upwork':               'suitcase',
        'zotero':               'book',

        'stackoverflow':        'stack-overflow',
        'google-openidconnect': 'google',
        'facebook-app':         'facebook',
        'vimeo':                'vimeo-square',
        'live':                 'windows',

        'email':                'envelope',
        'username':             'user',
    }

    for suffix in ('-oauth', '-oauth2'):
        if key.endswith(suffix):
            key = key.replace(suffix,'')

    while key in mapping:
        key = mapping[key]

    return key

################################################################################

@register.filter
def backend_name(backend):
    name = backend.__name__
    name = name.replace('OAuth', ' OAuth')
    name = name.replace('OpenId', ' OpenId')
    name = name.replace('Sandbox', '')
    name = NAME_RE.sub(r'\1 Auth', name)
    return name


@register.filter
def backend_class(backend):
    return backend.name.replace('-', ' ')


@register.filter
def icon_name(name):
    return get_icon(name)

@register.filter
def social_backends(backends):
    return filter_backends(
        backends,
        lambda name, backend: name not in LEGACY_NAMES
    )

@register.filter
def legacy_backends(backends):
    return filter_backends(
        backends,
        lambda name, backend: name in LEGACY_NAMES
    )

@register.filter
def oauth_backends(backends):
    return filter_backends(
        backends,
        lambda name, backend: issubclass(backend, OAuthAuth)
    )

#*******************************************************************************

@register.filter
def filter_backends(backends, filter_func):
    backends = [item for item in backends.items() if filter_func(*item)]
    backends.sort(key=lambda backend: backend[0])
    return backends

#*******************************************************************************

@register.simple_tag(takes_context=True)
def associated(context, backend):
    user = context.get('user')
    context['association'] = None
    if user and user.is_authenticated():
        context['association'] = Storage.user.get_social_auth_for_user(
            user,
            backend.name
        ).first()
    return ''

################################################################################

#@register.simple_tag(takes_context=True)
#def widget(context, provider, alias=None, config={}, data={}):
#    context['widgets'] = context.get('widgets', {})
#
#    if provider not in context['widgets']:
#        context['widgets'][provider] = {}
#
#    if alias is None:
#        alias = '%s-%d' % (provider, len(context['widgets'][provider])+1)
#
#    if alias not in context['widgets'][provider]:
#        context['widgets'][provider][alias] = {
#            'config': config,
#            'data':   data,
#        }
#
#    if 'render' not in context['widgets'][provider][alias]:
#        tpl = loader.get_template('blocks/%s.html')
#
#        if tpl is not None:
#            context['widgets'][provider][alias]['render'] = tpl.render(
#                provider = provider,
#                alias    = alias,
#            **context['widgets'][provider][alias])
#
#    return context['widgets'][provider][alias]['render']

#*******************************************************************************

class Widget(object):
    def __init__(self, provider, alias=None, config={}, payload={}):
        self._prvd = provider
        self._name = alias
        self._conf = config
        self._data = payload

        self._uuid = uuid.uuid4().hex

    provider = property(lambda self: self._prvd)
    config   = property(lambda self: self._conf)

    alias    = property(lambda self: self._name)
    uniq_id  = property(lambda self: self._uuid)

    payload  = property(lambda self: self._data)
    data     = property(lambda self: self.payload.get('data', self.payload))
    results  = property(lambda self: self.payload.get('results', self.payload))

    @property
    def template(self):
        try:
            return loader.get_template('blocks/%s.html' % self.provider)
        except Exception,ex:
            raise ex

            return None

    @property
    def as_html(self):
        try:
            return self.template.render({
                'widget':   self,
                'provider': self.provider,

                'alias':    self.alias,
                'uniq_id':  self.uniq_id,

                'config':   self.config,

                'payload':  self.payload,
                'data':     self.data,
                'results':  self.results,
            })
        except Exception,ex:
            raise ex

            return ""

#*******************************************************************************

@register.filter(name='form_field')
def form_field(field, **kwargs):
   return field.as_widget(attrs=kwargs)

#*******************************************************************************

@register.filter
def widget(data, provider, alias=None, **config):
    obj = Widget(provider, alias, config, data)

    return obj.as_html

#*******************************************************************************

@register.filter
def json(data, pretty=False):
    return simplejson.dumps(data)

################################################################################

@register.filter
def human_number(value):
    return humanize.intword(value)

#*******************************************************************************

@register.filter
def human_size(value):
    return humanize.naturalsize(value)

#*******************************************************************************

@register.filter
def since(when):
    resp = humanize.naturalday(when)

    if resp.lower()=='today':
        resp = humanize.naturaltime(when)

    return resp

#*******************************************************************************

@register.filter
def price(amount,currency=u'€'):
    return u'{0:.2f}{1}'.format(amount, currency)

#*******************************************************************************

@register.filter
def percent(value,total):
    return u'{0:.2f}'.format(float(value)/float(total))

################################################################################

@register.filter
def slice_by(value, items):
    return [value[n:n + items] for n in range(0, len(value), items)]

#*******************************************************************************

@register.filter
def fa_icon(key):
    return get_icon(key)

#*******************************************************************************

@register.filter
def reverse(s):
    return s[::-1]

#*******************************************************************************

@register.filter
def capitalize(s):
    return s.ucfirst()

#*******************************************************************************

@register.filter
def media(*paths):
    return '/'.join(['/media']+[x for x in paths])

#*******************************************************************************

@register.filter
def static(*paths):
    return '/'.join(['/static']+[x for x in paths])


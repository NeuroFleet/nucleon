from nucleon.connector.shortcuts import *

#*******************************************************************************

def logout(request):
    """Logs out user"""
    auth_logout(request)
    return redirect('/')

#*******************************************************************************

@render_to('connector/home.html')
def home(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        return redirect('done')

#*******************************************************************************

@login_required
@render_to('connector/home.html')
def done(request):
    """Login complete view, displays user data"""
    pass

#*******************************************************************************

@render_to('connector/home.html')
def validation_sent(request):
    """Email validation sent confirmation page"""
    return {
        'validation_sent': True,
        'email': request.session.get('email_validation_address')
    }

#*******************************************************************************

@render_to('connector/home.html')
def require_email(request):
    """Email required page"""
    strategy = load_strategy()
    partial_token = request.GET.get('partial_token')
    partial = strategy.partial_load(partial_token)
    return {
        'email_required': True,
        'partial_backend_name': partial.backend,
        'partial_token': partial_token
    }

#*******************************************************************************

@psa('social:complete')
def ajax_auth(request, backend):
    """AJAX authentication endpoint"""
    if isinstance(request.backend, BaseOAuth1):
        token = {
            'oauth_token': request.REQUEST.get('access_token'),
            'oauth_token_secret': request.REQUEST.get('access_token_secret'),
        }
    elif isinstance(request.backend, BaseOAuth2):
        token = request.REQUEST.get('access_token')
    else:
        raise HttpResponseBadRequest('Wrong backend type')
    user = request.backend.do_auth(token, ajax=True)
    login(request, user)
    data = {'id': user.id, 'username': user.username}
    return HttpResponse(json.dumps(data), mimetype='application/json')

################################################################################

@login_required
@render_to('special/landing.html')
def landing(request):
    return {
        
    }

#*******************************************************************************

@login_required
@render_to('connector/profile.html')
def user_profile(request):
    return {
        
    }

#*******************************************************************************

@login_required
def dynamic_page(request, section, chemin):
    section = {
        'search': 'search',
        'status': 'status',

        'theme':  'skin',

        'layout': 'layouts',
        'widget': 'widgets',
    }.get(section, 'connector/%s' % section)

    return render_template(request, '%s/%s.html' % (section,chemin))

################################################################################

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^login/$',                   home),
    url(r'^logout/$',                  logout),
    url(r'^email/$',                   require_email, name='require_email'),
    url(r'^email-sent/',               validation_sent),
    url(r'^ajax/(?P<backend>[^/]+)/$', ajax_auth, name='ajax-auth'),

    url(r'^apis$',                     done, name='done'),
    url(r'^profile/$',                 user_profile),

    #url(r'^([^/]+)/([^/]+).html$',          app_views.dynamic_page),
]


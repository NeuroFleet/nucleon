from nucleon.connector.utils import *

################################################################################

def agora_google(pseudo, creds, target='me'):
    req = requests.get('https://www.googleapis.com/plus/v1/people/me/people/visible', params={
        'access_token': creds['access_token'],
    })

    resp = req.json()

    profile = dict(id=1, name="Me")

    print resp

    for person in resp['items']:
        Reactor.wamp.publish(u"reactor.perso.social.knows", 'google', profile, person, RATINGs[1])

#*******************************************************************************

def agora_facebook(pseudo, creds, target='me', depth=0):
    if (3 < depth): return

    profile = cache.get('facebook-%s-profile' % target)

    if profile is None:
        profile = requests.get('https://graph.facebook.com/%s/' % target, params={
            'fields':       'id,name,location,picture',
            'access_token': creds['access_token'],
        }).json()

        cache.set('facebook-%s-profile' % target, profile, timeout=500)

    Reactor.wamp.publish(u"reactor.perso.social.person", 'facebook', profile)

    friends = cache.get('facebook-%s-friends' % target)

    if friends is None:
        req = requests.get('https://graph.facebook.com/%s/friends' % target, params={
            'fields':       'id,name,location,picture',
            'access_token': creds['access_token'],
        })

        friends = req.json()['data']

        cache.set('facebook-%s-friends' % target, friends, timeout=500)

    for person in friends:
        Reactor.rq.enqueue(agora_facebook, 'social')(pseudo, creds, person['id'], depth+1)

        Reactor.wamp.publish(u"reactor.perso.social.knows", 'facebook', profile, person, RATINGs[depth+1])

#*******************************************************************************

def agora_instagram(pseudo, creds, target='self', depth=0):
    if (3 < depth): return

    profile = requests.get('https://api.instagram.com/v1/users/%s/' % target, params={
        'access_token': creds['access_token'],
    }).json()

    Reactor.wamp.publish(u"reactor.perso.social.person", 'instagram', profile)

    req = requests.get('https://api.instagram.com/v1/users/%s/follows' % target, params={
        'access_token': creds['access_token'],
    })

    resp = req.json()

    for person in resp['data']:
        Reactor.rq.enqueue(agora_instagram, 'social')(pseudo, creds, person['id'], depth+1)

        Reactor.wamp.publish(u"reactor.perso.social.knows", 'instagram', profile, person, RATINGs[depth+1])


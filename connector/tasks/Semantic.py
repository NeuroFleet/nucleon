from nucleon.connector.shortcuts import *

################################################################################

@Reactork.task.register_task('foaf')
class Friend_of_a_Friend(Reactork.task.Walker):
    mapping = {
        'person': {
            'graph': {
                'node': 'person',
                'narrow': {
                    'network': 'network',
                    'uid':     'uid',
                },
            },
        },
        'knows': {
            'graph': {
                'edge': 'knows',
                'narrow': {
                },
            },
        },
    }

    def initialize(self):
        pass

    #***************************************************************************

    def discover(self, identity):
        pass


from reactor.shortcuts import *

#*******************************************************************************

#import hipchat

#from slacksocket import SlackSocket

################################################################################

@Reactor.wamp.register_middleware('perso.Talker.HipChat')
class HipChat(Profiler):
    def on_init(self, details):
        self.cnx = hipchat.HipChat(token=settings.API_HIPCHAT_TOKEN)

    #***************************************************************************

    def mainloop(self):
        for entry in self.cnx.events():
            self.publish('reactor.perso.talk.hipchat.event', entry.json)

################################################################################

@Reactor.wamp.register_middleware('perso.Talker.Slack')
class Slack(Profiler):
    def on_init(self, details):
        self.cnx = SlackSocket(settings.API_SLACK_TOKEN,translate=True) # translate will lookup and replace user and channel IDs with their human-readable names. default true. 

    #***************************************************************************

    def mainloop(self):
        for entry in self.cnx.events():
            self.publish('reactor.perso.talk.slack.event', entry.json)

    ############################################################################

    MESSAGE_EVENTs = ['message']+[
        'message.%s' % x
        for x in ('channels','groups','im','mpim')
    ]+[
        'im.%s' % x
        for x in ('created','open')
    ]

    PRESENCE_EVENTs = [
        'presence_change',
        'user_typing',
    ]+[
        x+'_'+y
        for x in ('team','group','channel')
        for y in ('joined','left')
    ]

    @Reactor.wamp.register_topic(u'perso.talk.slack.event')
    def on_event(self, data):
        kind = data['type'] ; del data['type']

        if kind in self.MESSAGE_EVENTs:
            self.publish('reactor.perso.talk.slack.text', kind, data['user'], data['text'])

            for link in []:
                self.publish('reactor.perso.talk.slack.link', data['user'], link)
        elif kind in ['file_created']:
            self.publish('reactor.perso.talk.slack.file', data['user'], data['text'])

        elif kind in self.PRESENCE_EVENTs:
            where,how = kind.split('_',1)

            where,how = {
                'presence': lambda w,h: ('user','presence'),
            }.get(where,lambda w,h: (w,h))(where,how)

            self.publish('reactor.perso.talk.slack.'+where, data['user'], how, data)
        else:
            self.publish('reactor.perso.talk.slack.event', kind, data)

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.talk.slack.event')
    def on_meta(self, kind, data):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.talk.slack.channel')
    def on_channel(self, who, where, how):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'perso.talk.slack.message.text')
    def on_text(self, peer, msg):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.talk.slack.message.link')
    def on_link(self, peer, url):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.talk.slack.message.file')
    def on_file(self, peer, vfs):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'perso.mail.box.list')
    def box_list(self):
        return self.boxes.keys()



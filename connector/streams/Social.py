from reactor.shortcuts import *

RATINGs = {
    1: 'first',
    2: 'second',
    3: 'third',
    4: 'forth',
    5: 'fifth',
    6: 'sixth',
}

################################################################################

from pattern.web import Mail, GMAIL, SUBJECT

@Reactor.wamp.register_middleware('perso.Social.People')
class People(Profiler):
    def on_init(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'reactor.perso.social.person')
    def on_person(self, pseudo, profile={}):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'reactor.perso.social.relation')
    def on_knows(self, source, target, details={}):
        pass

    ############################################################################

    mapping = [
        'facebook','instagram',
        #'google-oauth2','linkedin-oauth2',
    ]

    @Reactor.wamp.register_method(u'reactor.perso.social.agora')
    def search(self, pseudo):
        from nucleon.connector import tasks

        #Reactor.rq.empty('social')

        from django.contrib.auth.models import User

        qs = User.objects.filter(username=pseudo)

        if len(qs):
            resp = dict(success=[], fail=[])

            for alias in self.mapping:
                narrow = alias

                for word in ('-oauth2', '-oauth'):
                    while (word in narrow):
                        narrow = narrow.replace(word, '')

                handler = getattr(tasks, 'agora_%s' % narrow, None)

                try:
                    backend = qs[0].social_auth.get(provider=alias)
                except:
                    backend = None

                if callable(handler) and (backend is not None):
                    Reactor.rq.enqueue(handler, 'social')(qs[0].username, backend.extra_data)

                    resp['success'] += [alias]
                else:
                    resp['fail'] += [alias]

            return resp
        else:
            return "error"


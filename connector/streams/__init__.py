from reactor.shortcuts import *

################################################################################

import pocket

@Reactor.wamp.register_middleware('media.Web.Web.Bookmarks')
class Bookmarks(Profiler):
    def on_init(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'perso.bookmarks.link')
    def pocket_link(self, link):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.bookmarks.article')
    def pocket_article(self, link):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.bookmarks.image')
    def pocket_image(self, link):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.bookmarks.video')
    def pocket_video(self, link):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'perso.bookmarks.listing')
    def bookmark_listing(self):
        return [entry for entry in self.listing('web_bookmark')]

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.bookmarks.retrieve')
    def bookmark_retrieve(self, link):
        return self.read('web_bookmark', {
            'link': link,
        })

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.bookmarks.append')
    def bookmark_append(self, link):
        target = urlparse(link)

        doc = self.upsert('web_bookmark', {
            'link':   link,
        },{
            'domain': target.netloc,
            'path':   target.path,
        })

        return doc

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.bookmarks.remove')
    def bookmark_remove(self, link):
        self.delete('web_bookmark', {
            'link':   link,
        })

    ############################################################################

    @Reactor.wamp.register_method(u'perso.bookmarks.pocket')
    def pocket_import(self, api_key, api_pki):
        cnx = pocket.Pocket(api_key, api_pki)

        for i in range(0, 100):
            self.queue.spawn(self.pocket_query, cnx, 0)

    #***************************************************************************

    # 'state','favorite','tag','contentType','sort','detailType','search','domain','since','count','offset')
    def pocket_query(self, cnx, offset):
        resp = cnx.get(
            state      = 'all',
            detailType = 'complete',
        )

        for uid,obj in resp['list'].iteritems():
            target = urlparse(obj)

            doc = self.upsert('web_bookmark', {
                'link':      obj['resolved_url'],
            },{
                'domain':    target.netloc,
                'path':      target.path,

                'favorite':  obj['favorite'],
                'pocket_id': obj['item_id'],
            })

            if obj['is_article']=='1':
                self.publish(u'perso.bookmarks.article', obj['resolved_url'], {
                    'label':   obj['given_title'],
                    'title':   obj['resolved_title'],
                    'excerpt': obj['excerpt'],
                    'tags':    obj['tags'],
                    'words':   obj['word_count'],
                })

            for img in obj['images'].values():
                cnt = dict([(x, img.get(x,'')) for x in ('caption','credit')])

                self.publish(u'perso.bookmarks.image', img['src'], img['width'], img['height'], **cnt)

            for clip in obj['videos'].values():
                self.publish(u'perso.bookmarks.video', clip['src'], clip['width'], clip['height'])

################################################################################

from . import Social

from . import Talker

from . import Telecom


from reactor.shortcuts import *

################################################################################

from pattern.web import Mail, GMAIL, SUBJECT

@Reactor.wamp.register_middleware('perso.Telecom.MailBox')
class MailBox(Profiler):
    def on_init(self, details):
        self.boxes = {
            'eclectic.brain@gmail.com': dict(
                password = '',
            ),
            'shinobi.py@gmail.com': dict(
                password = '',
            ),
            'it.samurai.squad@gmail.com': dict(
                password = '',
            ),
        }

    ############################################################################

    @Reactor.wamp.register_topic(u'perso.mail.update')
    def on_sync(self, box):
        pass

    #***************************************************************************

    @Reactor.wamp.register_topic(u'perso.mail.import')
    def on_new_mail(self, box, mail):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'perso.mail.box.list')
    def box_list(self):
        return self.boxes.keys()

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.mail.box.folders')
    def box_dirs(self):
        pass

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.mail.box.sync')
    def box_sync(self):
        for box,cfg in self.boxes.iteritems():
            cfg['service'] = cfg.get('service', GMAIL)

            proxy = Mail(username=box, password=cfg['password'], service=cfg['service'])

            for alias,folder in proxy.folders.iteritems():
                try:
                    lst = [uid for uid in folder.search('')]
                except Exception,ex:
                    lst = []

                if alias in proxy.folders:
                    self.queue.spawn(self.box_sync_folder, box, proxy, alias, *lst)

            self.publish('reactor.perso.mail.update', box)

    #***************************************************************************

    def box_sync_folder(self, box, proxy, alias, *targets):
        directory = proxy.folders[alias]

        for uid in targets:
            msg = directory.read(uid)

            self.publish('reactor.perso.mail.import', box, uid, msg)

            #upsert(Mail_Message, *Mail_Message.from_mail(box.owner.pk, msg))

        print "*) Processed Mail message '%(username)s' at : %(hostname)s" % box.__dict__

    ############################################################################

    @Reactor.wamp.register_method(u'perso.mail.message.list')
    def message_list(self, box, folder='INBOX'):
        box = self.box_wrapper(box)

        return []

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.mail.message.read')
    def message_read(self, box, uid):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'perso.mail.attach.list')
    def attach_list(self, box, uid):
        pass

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.mail.attach.read')
    def attach_read(self, box, uid, fid):
        pass

    #***************************************************************************

    @Reactor.wamp.register_method(u'perso.mail.attach.file')
    def attach_file(self, box, uid, fid):
        pass

################################################################################

#@Reactor.rq.register_task(queue='perso')
def refresh_wordpress(wp_id, **response):
    site = WordpressSite.objects.get(pk=wp_id)

    for entry in site.get_posts():
        upsert(ArticlePost, *ArticlePost.from_wordpress(site.pk, entry))

        rss.title      = rss.proxy.feed.title
        rss.summary    = rss.proxy.feed.description

        site.save()

        print "*) Processed WP site : %(link)s" % rss.__dict__
    else:
        print "*) Skipped WP site : %(link)s" % rss.__dict__


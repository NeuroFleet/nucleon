from nucleon.infosec.shortcuts import *

from netaddr import IPNetwork, IPAddress, IPRange

################################################################################

@Reactor.orm.register_model('network_provider')
class NetworkProvider(models.Model):
    alias = models.CharField(max_length=64)
    title = models.CharField(max_length=256, blank=True)
    roles = models.CharField(max_length=64, choices=[
        ('cloud',    "Cloud service"),
        ('infosec',  "InfoSec company"),
        ('hosting',  "Hosting Provider"),
        ('platform', "Platform & Infrastructure"),
        ('network',  "Next-Gen networking"),
        ('other',    "Other organizations"),
    ], default='other')

    def check_addr(self, target):
        endpoint = IPAddress(target)

        for query in [self.ip_masks, self.ip_ranges]:
            for slot in query.all():
                if slot.check_addr(endpoint):
                    return True

        return False

#*******************************************************************************

@Reactor.orm.register_model('network_provider_range')
class NetworkProviderRange(models.Model):
    provider   = models.ForeignKey(NetworkProvider, related_name='ip_ranges')

    start_ip   = models.GenericIPAddressField(protocol='both', unpack_ipv4=False)
    end_ip     = models.GenericIPAddressField(protocol='both', unpack_ipv4=False)

    def check_addr(self, endpoint):
        return (endpoint in self.wrap_range)

    wrap_range = property(lambda self: IPRange(self.start_ip, self.end_ip))

    wrap_start = property(lambda self: IPAddress(self.start_ip))
    wrap_end   = property(lambda self: IPAddress(self.end_ip))

#*******************************************************************************

@Reactor.orm.register_model('network_provider_mask')
class NetworkProviderMask(models.Model):
    provider = models.ForeignKey(NetworkProvider, related_name='ip_masks')
    address  = models.CharField(max_length=256)

    def check_addr(self, endpoint):
        return (endpoint in self.wrapper)

    wrapper  = property(lambda self: IPNetwork(self.address))


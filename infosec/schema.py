from reactor.webapp.shortcuts import *

from .models import *

################################################################################

@Reactor.mongo.register_embed('scan_host_banned')
class ScanHostBanned(MongoDB.EmbeddedDocument):
    address  = MongoDB.StringField(required=True)
    provider = MongoDB.StringField(required=True)

    wrapper  = property(lambda self: IPAddress(self.address))

#*******************************************************************************

@Reactor.mongo.register_embed('scan_host_list')
class ScanHostList(MongoDB.Document):
    module       = MongoDB.StringField(max_length=48)
    executed_at  = MongoDB.DateTimeField(default=lambda:datetime.now())

    blacklisted  = MongoDB.EmbeddedDocumentListField(ScanHostBanned)

#*******************************************************************************

@Reactor.mongo.register_document('scan_host_result')
class ScanHostResult(MongoDB.Document):
    parent_list  = MongoDB.ReferenceField(ScanHostList)

    target_addr  = MongoDB.StringField(required=True)
    target_ip    = property(lambda self: IPAddress(self.target_addr))

    started_at   = MongoDB.DateTimeField(default=lambda:datetime.now())
    finished_at  = MongoDB.DateTimeField(required=False, null=True, default=None)

    standart_out = MongoDB.StringField(required=False, null=True, default=None)
    standart_err = MongoDB.StringField(required=False, null=True, default=None)

    scanned_data = MongoDB.StringField(required=False, default={})

    def check(self):
        if self.id:
            from .tasks import scan_host

            return Reactor.rq.enqueue(scan_host, self.id)

    @classmethod
    def post_save(cls, sender, document, **kwargs):
        if 'created' in kwargs:
            if kwargs['created']:
                pass # self.check()


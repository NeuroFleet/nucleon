from nucleon.infosec.utils import *

from nucleon.infosec.models import *
from nucleon.infosec.schema import *

def scan_addr(list_id, target):
    current = ScanHostList.objects.get({'id': list_id})

    for provider in NetworkProvider.objects.all():
        if provider.check_addr(target):
            smith = ScanHostBanned(
                address  = target,
                provider = provider.alias,
            )

            try:
                current.blacklisted.create(smith)
            except Exception,ex:
                raise ex

                pass

            current.save()

            return False

    host = ScanHostResult.objects.get_or_create(
        parent_list = lst,
        target_addr = target,
    )

    host.save()

    Reactor.rq.enqueue(scan_host, host.id).wait()

    return True

MODULEs = {
    'nmap': {
        'prefix': 'net',
        'folder': 'nmap',
        'script': 'nmap',
        'params': '-T4 -F %s',
    },
    'msf': {
        'prefix': 'msf',
        'folder': 'metasploit',
        'script': 'msf-cli',
        'params': 'scan %s',
    },
    'w3af': {
        'prefix': 'w3af',
        'folder': 'w3af-kit',
        'script': 'w3af_cli',
        'params': 'scan -i %s',
    },
    'cfm': {
        'prefix': 'cfm',
        'folder': 'coldfusion',
        'script': 'python',
        'params': 'cold.py %s',
    },
}

def scan_host(host_id):
    result = ScanHostResult.objects.get({'id': host_id})

    mod = MODULEs.get(result.module, MODULEs['nmap'])

    stmt = "%(script)s %(params)s" % mod

    p = Popen(stmt % dest, shell=True, stdout=PIPE, stderr=PIPE)

    p.wait() ; result.finished_at = datetime.now()

    result.return_code  = p.code
    result.standart_out = p.stdout.read()
    result.standart_err = p.stderr.read()

    result.save()


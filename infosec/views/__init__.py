from nucleon.infosec.shortcuts import *

#*******************************************************************************

@render_to('pages/home.html')
def homepage(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        return redirect('done')

################################################################################

from . import Scanner

################################################################################

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^scanner/$', Scanner.homepage),

    url(r'^$',         homepage),
]


from reactor.webapp.shortcuts import *

#*******************************************************************************

from bulbs       import property         as Neo4Prop
from bulbs.utils import current_datetime as Neo4Now

################################################################################

@Reactor.graph.register_node('host')
class TargetHost(Reactor.graph.Node):
    address   = Neo4Prop.String(nullable=False)
    fullname  = Neo4Prop.String()

#*******************************************************************************

@Reactor.graph.register_node('router')
class v4Router(Reactor.graph.Node):
    address   = Neo4Prop.String(nullable=False)
    fullname  = Neo4Prop.String()

################################################################################

@Reactor.graph.register_edge('uplink')
class Uplink(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)
    services  = Neo4Prop.Dictionary(default={}, nullable=False)

#*******************************************************************************

@Reactor.graph.register_edge('near-by')
class NearBy(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)
    profile   = Neo4Prop.Dictionary(default={}, nullable=False)


from django.contrib import admin

from .models import *

################################################################################

class RangeAdmin(admin.TabularInline):
    model = NetworkProviderRange

class MaskAdmin(admin.TabularInline):
    model = NetworkProviderMask

#*******************************************************************************

class ProviderAdmin(admin.ModelAdmin):
    list_display  = ('alias','title','roles')
    list_filter   = ('alias','title','roles')
    list_editable = ('title','roles')

    inlines       = (RangeAdmin, MaskAdmin)

admin.site.register(NetworkProvider, ProviderAdmin)


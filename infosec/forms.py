from nucleon.infosec.utils import *

################################################################################

class CrawlerForm(forms.Form):
    spider   = forms.ChoiceField(choices=[
        ('generic', "Generic Spider"),
    ])
    starters = forms.CharField(max_length=256)

################################################################################

class SemanticForm(forms.Form):
    source   = forms.CharField(widget=forms.Textarea)
    spider   = forms.ChoiceField(choices=[
        ('generic', "Generic Spider"),
    ])


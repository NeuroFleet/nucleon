from reactor.webapp.shortcuts import *

#*******************************************************************************

from bulbs       import property         as Neo4Prop
from bulbs.utils import current_datetime as Neo4Now

################################################################################

@Reactor.graph.register_node('organization')
class Organization(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

#*******************************************************************************

@Reactor.graph.register_node('project')
class Project(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

################################################################################

@Reactor.graph.register_edge('member-of')
class MemberOf(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)

    roles     = Neo4Prop.Dictionary(default={}, nullable=False)

#*******************************************************************************

@Reactor.graph.register_edge('work-on')
class WorkOn(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)

    roles     = Neo4Prop.Dictionary(default={}, nullable=False)


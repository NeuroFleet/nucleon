from django.contrib import admin

# Register your models here.
from .models import *

################################################################################

class SshInline(admin.TabularInline):
    model = IdentitySSH

#*******************************************************************************

class PgpInline(admin.TabularInline):
    model = IdentityPGP

################################################################################



from nucleon.console.shortcuts import *

################################################################################

@Reactor.orm.register_model('identity_ssh')
class IdentitySSH(models.Model):
    owner   = models.ForeignKey(Identity, related_name='ssh_keys')
    alias   = models.CharField(max_length=48, blank=True)

    public  = models.TextField()
    private = models.TextField(blank=True)

#*******************************************************************************

@Reactor.orm.register_model('identity_pgp')
class IdentityPGP(models.Model):
    owner   = models.ForeignKey(Identity, related_name='pgp_keys')
    alias   = models.CharField(max_length=48, blank=True)

    public  = models.TextField()
    private = models.TextField(blank=True)


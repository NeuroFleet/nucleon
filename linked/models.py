from nucleon.linked.shortcuts import *

################################################################################

class IdentityLDF(models.Model):
    owner    = models.ForeignKey(Identity, related_name='ldf')
    alias    = models.CharField(max_length=48, blank=True)

    endpoint = models.CharField(max_length=256)

#*******************************************************************************

class IdentityLDP(models.Model):
    owner    = models.ForeignKey(Identity, related_name='ldp')
    alias    = models.CharField(max_length=48, blank=True)

    endpoint = models.CharField(max_length=256)

################################################################################

class RdfNamespace(models.Model):
    alias     = models.CharField(max_length=24)
    title     = models.CharField(max_length=128, blank=True)

    address   = models.CharField(max_length=256)
    is_custom = models.BooleanField(default=False)

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "RDF namespace"
        verbose_name_plural = "RDF namespaces"

#*******************************************************************************

class XsdType(models.Model):
    alias     = models.CharField(max_length=24)
    title     = models.CharField(max_length=128, blank=True)

    namespace = models.ForeignKey(RdfNamespace, related_name='datatypes', blank=True, null=True, default=None)
    creator   = models.ForeignKey(Identity, related_name='datatypes', blank=True, null=True, default=None)

################################################################################

class OwlOntology(models.Model):
    alias     = models.CharField(max_length=64)
    title     = models.CharField(max_length=128, blank=True)

    address   = models.CharField(max_length=256)
    content   = models.TextField(blank=True)

    namespace = models.ForeignKey(RdfNamespace, related_name='ontologies', blank=True, null=True, default=None)
    creator   = models.ForeignKey(Identity, related_name='ontologies', blank=True, null=True, default=None)

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "OwL ontology"
        verbose_name_plural = "OwL ontologies"

##*******************************************************************************

#class OwlPredicate(models.Model):
#    ontology = models.ForeignKey(RdfOntology, related_name='predicates')
#
#    alias    = models.CharField(max_length=64)
#    title    = models.CharField(max_length=128, blank=True)
#
#    xsd_type = models.ForeignKey(DataType, related_name='predicates')
#
#    creator  = models.ForeignKey(Identity, related_name='predicates', blank=True, null=True, default=None)
#
###*******************************************************************************
#
#class OwlRelation(models.Model):
#    ontology = models.ForeignKey(RdfOntology, related_name='relations')
#
#    alias    = models.CharField(max_length=64)
#    title    = models.CharField(max_length=128, blank=True)
#
#    address  = models.CharField(max_length=256)
#
#    creator  = models.ForeignKey(Identity, related_name='relations', blank=True, null=True, default=None)

################################################################################

@Reactor.orm.register_model('sparql_endpoint')
class SparqlEndpoint(models.Model):
    alias      = models.CharField(max_length=64)
    link       = models.CharField(max_length=256)

    namespaces = models.ManyToManyField(RdfNamespace, related_name='endpoints')
    creator    = models.ForeignKey(Identity, related_name='sparql_ep', blank=True, null=True, default=None)

    ns_listing = property(lambda self: ' , '.join([ns.alias for ns in self.namespaces.all()]))
    prefixes   = property(lambda self: '\n'.join([
        'PREFIX %(alias)s: <%(link)s>' % ns.__dict__
        for ns in self.namespaces.all()
    ]))

    #***************************************************************************

    def query(self, statement, *args, **kwargs):
        cmd = SPARQLWrapper(self.link)

        try:
            statement = statement % args
        except:
            try:
                statement = statement % kwargs
            except:
                pass

        statement = self.prefixes + '\n' + statement

        cmd.setQuery(statement)

        cmd.setReturnFormat(JSON)

        return cmd

    def execute(self, statement, *args, **kwargs):
        cmd = self.query(statement, *args, **kwargs)

        try:
            return cmd.query().convert()
        except:
            return None

    #***************************************************************************

    #class Wrapper(DataSet):
    #    COLLECTION = 'sparql_ontology'
    #
    #    def initialize(self):
    #        self.persist()
    #
    #    def persist(self):
    #        cfg = dict(endpoint=self.link, narrow=self.narrow, ontology=self.data['class'], ancestor=self.data['parent'])
    #
    #        doc = None
    #
    #        try:
    #            doc = self.tribe.insert_one(cfg)
    #        except:
    #            pass
    #
    #        #doc.save()
    #
    #        return doc

    #***************************************************************************

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "SPARQL endpoint"
        verbose_name_plural = "SPARQL endpoints"

#*******************************************************************************

@Reactor.orm.register_model('sparql_ontology')
class SparqlOntology(models.Model):
    endpoint  = models.ForeignKey(SparqlEndpoint, related_name='ontologies')
    alias     = models.CharField(max_length=512)

    narrow    = models.TextField(blank=True)
    classe    = models.TextField(blank=True)
    parent    = models.TextField(blank=True)

    def __str__(self):     return str(self.narrow)
    def __unicode__(self): return unicode(self.narrow)

    class Meta:
        verbose_name        = "SparQL ontology"
        verbose_name_plural = "SparQL ontologies"

#*******************************************************************************

@Reactor.orm.register_model('sparql_query')
class SparqlQuery(models.Model):
    endpoint   = models.ForeignKey(SparqlEndpoint, related_name='queries')
    alias      = models.CharField(max_length=128)

    creator    = models.ForeignKey(Identity, related_name='sparql_db', blank=True, null=True, default=None)

    statement  = models.TextField()
    namespaces = models.ManyToManyField(RdfNamespace, related_name='queries')

    prefixes   = property(lambda self: [ns.alias for ns in self.namespaces.all()])

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "SPARQL query"
        verbose_name_plural = "SPARQL queries"

################################################################################

@Reactor.orm.register_model('ckan_datahub')
class CKAN_DataHub(models.Model): # CKAN backend
    alias   = models.CharField(max_length=64)
    realm   = models.CharField(max_length=256)
    version = models.PositiveIntegerField(default=3)

    #***************************************************************************

    def rpath(self, *parts): return '/'.join([self.realm, 'api', str(self.version)]+[x for x in parts])

    def request(self, verb, *path, **kwargs):
        handler = getattr(requests, verb.lower(), None)

        if callable(handler):
            req = handler(self.rpath(*path), **kwargs)

            return req.json()
        else:
            return {}

    #***************************************************************************

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Meta:
        verbose_name        = "CKAN DataHub"
        verbose_name_plural = "CKAN DataHubs"


from django.contrib import admin

# Register your models here.
from .models import *

################################################################################

class LdpInline(admin.TabularInline):
    model = IdentityLDP

#*******************************************************************************

class LdfInline(admin.TabularInline):
    model = IdentityLDF

################################################################################

class NamespaceAdmin(admin.ModelAdmin):
    list_display  = ('alias', 'address')
    list_editable = ('address',)

admin.site.register(RdfNamespace, NamespaceAdmin)

#*******************************************************************************

class OntologicalAdmin(admin.ModelAdmin):
    list_display  = ('namespace', 'alias', 'address')
    list_filter   = ('namespace__alias',)
    list_editable = ('address',)

admin.site.register(OwlOntology, OntologicalAdmin)

##################################################################################################

class EndpointAdmin(admin.ModelAdmin):
    list_display  = ('alias', 'link', 'prefixes')
    list_filter   = ('namespaces__alias',)
    list_editable = ('link',)

admin.site.register(SparqlEndpoint, EndpointAdmin)

#*******************************************************************************

class OntologicalSparql(admin.ModelAdmin):
    list_display  = ('endpoint', 'alias', 'narrow', 'classe', 'parent')
    list_filter   = ('endpoint__alias',)

admin.site.register(SparqlOntology, OntologicalSparql)

#*******************************************************************************

class QueryAdmin(admin.ModelAdmin):
    list_display  = ('endpoint','alias', 'statement', 'prefixes')
    list_filter   = ('endpoint__alias','namespaces__alias',)
    list_editable = ('statement',)

admin.site.register(SparqlQuery, QueryAdmin)

##################################################################################################
##################################################################################################

class CKAN_HubAdmin(admin.ModelAdmin):
    list_display  = ('alias', 'realm', 'version')
    list_filter   = ('version',)
    list_editable = ('realm', 'version')

admin.site.register(CKAN_DataHub, CKAN_HubAdmin)


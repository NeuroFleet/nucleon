from nucleon.linked.utils import *

################################################################################

class SearchForm(forms.Form):
    q   = forms.CharField()

################################################################################

class SemanticForm(forms.Form):
    source   = forms.CharField(widget=forms.Textarea)
    spider   = forms.ChoiceField(choices=[
        ('generic', "Generic Spider"),
    ])


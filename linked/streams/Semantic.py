#-*- coding: utf-8 -*-

from reactor.shortcuts import *

import rdflib

################################################################################

@Reactor.wamp.register_middleware('daten.Semantic.RDF')
class RDF(Profiler):
    def on_init(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'<data>.rdf.any_method')
    def any_method(self, query, endpoint):
        pass

    URIRef    = rdflib.URIRef
    BNode     = rdflib.BNode
    Literal   = rdflib.Literal
    NS        = rdflib.Namespace
    Graph     = rdflib.Graph

    Prefix    = rdflib.namespace

    #*******************************************************************************

    class Descriptor(object):
        # OWL DC
        # SKOS XSD DCTERMS
        # RDF FOAF DOAP
        mapping = dict([
            (key.lower(),hnd)
            for key,hnd in [
                (x, getattr(rdflib.namespace, x))
                for x in rdflib.namespace.__all__
            ]
            if type(hnd) is rdflib.Namespace
        ]+[
            #('rdf',  rdflib.URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns') ),
            #('foaf', rdflib.URIRef(u'http://xmlns.com/foaf/0.1/')                 ),
        ])

        def prefix(self, ns, *path):
            if ns in self.mapping:
                resp = self.mapping[ns]

                for part in path:
                    if hasattr(resp, part):
                        resp = getattr(resp, part)
                    else:
                        return None

                return resp

            return None

        def literal(self, value):
            resp = value

            if type(resp) in (bool, int, long, float, str, unicode):
                resp = rdflib.Literal(resp)
            elif type(resp) in (tuple, set, frozenset, float, str, unicode):
                pass
            elif type(resp) in (dict,):
                pass

            return resp

        def populate(self, entry, graph=None):
            if graph is None:
                graph = rdflib.Graph()

            sbj = self.retrieve(entry.__dict__)

            for s,p,o in self.describe(entry, sbj):
                g.add( (s,p,o) )

            return graph

        def export(self, entry):
            grp = self.populate(entry)

            resp = grp.serialize(format='turtle')

            return resp

################################################################################

@Reactor.wamp.register_middleware('daten.Semantic.OWL')
class OwL(Profiler):
    def on_init(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'<data>.owl.any_method')
    def any_method(self, query, endpoint):
        pass

################################################################################

@Reactor.wamp.register_middleware('daten.Semantic.SparQL')
class SparQL(Profiler):
    def on_init(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'<data>.sparql.query')
    def sparql_query(self, query, endpoint):
        data = ep.execute('''SELECT ?target ?classe ?parent
WHERE {
  ?target rdf:type ?classe.
  ?target rdfs:subClassOf ?parent
}
GROUP BY ?target''')

        if type(data) in (str,unicode):
            pass
        elif type(data) in (dict,):
            if data['results']:
                print "#) Getting list of RDF ontologies on '%s' :" % ep.link

                for row in data['results']['bindings']:
                    print "\t-> %(value)s ..." % row['target']

                    onto,st = ep.ontologies.get_or_create(alias=row['target']['value'])

                    onto.narrow = row['target']['value']
                    onto.classe = row['classe']['value']
                    onto.parent = row['parent']['value']

                    #onto.narrow = json.dumps(row['target'])
                    #onto.classe = json.dumps(row['classe'])
                    #onto.parent = json.dumps(row['parent'])

                    onto.save()

                    #self.process(self.Wrapper(self, row['target']['value'], row))

    ############################################################################

    def request_raw(self, endpoint, query, **params):
        cnx = SparqlWrapper(endpoint)

        cnx.setQuery(query)

        cnx.setReturnFormat(SparqlJSON)

        return cnx.query().convert()

    #***************************************************************************

    def request_table(self, endpoint, query, **params):
        cols, rows = {}, []

        results = self.sparql_raw(endpoint, query, **params)

        for result in results["results"]["bindings"]:
            entry = {}

            for field in result.keys():
                cols[field] = field

                rows.append(result)

            print(result)

        return cols, rows

    #***************************************************************************

    def request_mime(self, endpoint, mimetype, query, **params):
        req = requests.get(endpoint, params={
            'default-graph-uri':    'http://dbpedia.org',
            'query':                query,
            'format':               mimetype,
            'timeout':              '30000',
            'debug':                'on',
            'CXML_redir_for_subjs': '121',
            'CXML_redir_for_hrefs': '',
        })

        return req.text

    #***************************************************************************

    def keyword(self, literal, roles=['NN']):
        qs = [
            'PREFIX owl: <http://www.w3.org/2002/07/owl#>',
            'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>',
            'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>',
            'PREFIX foaf: <http://xmlns.com/foaf/0.1/>',
            'PREFIX skos: <http://www.w3.org/2004/02/skos/core#>',
            'PREFIX quepy: <http://www.machinalis.com/quepy#>',
            'PREFIX dbpedia: <http://dbpedia.org/ontology/>',
            'PREFIX dbpprop: <http://dbpedia.org/property/>',
            'PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>',
            'PREFIX type: <http://dbpedia.org/class/yago/>',
            'PREFIX prop: <http://dbpedia.org/property/>',
            '',
        ]

        qs += ['SELECT ?word, ?link, ?type WHERE {']

        qs += ['    ?link rdf:type ?type.']
        qs += ['    ?link rdfs:label "%s"@en.']
        qs += ['    ?link rdfs:label ?word.']

        qs += ['}']

        qs = '\n'.join(qs)

        print qs

        ds = self.sparql_raw(qs)

        #ds['']        

        return ds


from nucleon.linked.shortcuts import *

#*******************************************************************************

#@login_required
@render_to('linked/home.html')
def homepage(request):
    return {
        'stats': [
    dict(color='red',    icon='link',     label="RDF namespaces",    value=0, total=500),
    dict(color='blue',   icon='code',     label="RDF ontologies",    value=0, total=1000000),
    dict(color='green',  icon='database', label="SPARQL ontologies", value=0, total=2500000),
    dict(color='purple', icon='plug',     label="SPARQL endpoints",  value=0, total=250),

    dict(color='green',  icon='sitemap',   label="CKAN Hubs",      value=0, total=50),
    dict(color='blue',   icon='archive',   label="CKAN Datasets",  value=0, total=100000),
    dict(color='red',    icon='briefcase', label="CKAN Resources", value=0, total=25000000),
    #dict(color='purple', icon='sitemap',   label="Docker Networks",   value=0, total=20),
        ],
    }

################################################################################

from . import Search

################################################################################

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^search/(?P<facet>[^/]+)$',       Search.perform,    name='search'),

    url(r'^$',                              homepage),
]


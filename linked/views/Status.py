from nucleon.console.shortcuts import *

################################################################################

@login_required
@render_to('status/contain.html')
def do_contain(request):
    #django_rq.enqueue(func, foo, bar=baz)

    #import docker

    #prx = docker.Client()

    return {
        'stats': [
    dict(color='blue',   icon='image',   label="Docker Images",     value=0, total=150),
    dict(color='red',    icon='cubes',   label="Docker Containers", value=0, total=50),
    dict(color='green',  icon='disk',    label="Docker Volumes",    value=0, total=100),
    dict(color='purple', icon='sitemap', label="Docker Networks",   value=0, total=20),
        ],
    }

#*******************************************************************************

@login_required
@render_to('status/monitor.html')
def do_monitor(request):
    #django_rq.enqueue(func, foo, bar=baz)

    return {
        'stats': [
    dict(color='blue',   icon='image',   label="Docker Images",     value=0, total=150),
    dict(color='red',    icon='cubes',   label="Docker Containers", value=0, total=50),
    dict(color='green',  icon='disk',    label="Docker Volumes",    value=0, total=100),
    dict(color='purple', icon='sitemap', label="Docker Networks",   value=0, total=20),
        ],
    }

#*******************************************************************************

@login_required
@render_to('status/streams.html')
def do_streams(request):
    #django_rq.enqueue(func, foo, bar=baz)

    return {
        'stats': [
    dict(color='blue',   icon='bullhorn', label="WAMP topics",      value=len(Reactor.wamp.topics),  total=700),
    dict(color='red',    icon='link',     label="WAMP methods",     value=len(Reactor.wamp.methods), total=400),
    dict(color='green',  icon='plug',     label="WAMP middlewares", value=len(Reactor.wamp.classes), total=40),
    dict(color='purple', icon='bookmark', label="WAMP sessions",    value=0, total=250),
        ],
    }


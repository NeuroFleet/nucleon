from nucleon.linked.shortcuts import *

################################################################################

Config = {
    'web': dict(
    ),
    'people': dict(
    ),
}

################################################################################

#@login_required
@render_to('linked/search.html')
def perform(request, facet='web'):
    resp = {
        'facet': facet,
        'stats': [
    dict(color='blue',   icon='image',   label="Docker Images",     value=0, total=150),
    dict(color='red',    icon='cubes',   label="Docker Containers", value=0, total=50),
    dict(color='green',  icon='disk',    label="Docker Volumes",    value=0, total=100),
    dict(color='purple', icon='sitemap', label="Docker Networks",   value=0, total=20),
        ],
        'views': dict([
            (key,'linked/%s/%s.html' % (facet,key))
            for key in ('form','side','show','help')
        ]),
        'frm_search': SearchForm(request.GET),
    }

    if ('q' in request.GET) or request.method=='POST':
        if resp['frm_search'].is_valid():
            resp['result'] = SearchForm(request.GET)

    return resp


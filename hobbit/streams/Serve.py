from reactor.shortcuts import *

################################################################################

@Reactor.wamp.register_middleware('media.Video.Watch.LiveStream')
class LiveStream(Profiler):
    def on_init(self, details):
        pass # trakt.tv.setup(apikey='')

    ############################################################################

    @Reactor.wamp.register_topic(u'media.video.live.watched')
    def on_watched(self, movie):
        pass

    ############################################################################

    @Reactor.wamp.register_method(u'media.video.live.stream')
    def stream(self, url, quality='best', player='mplayer'):
        os.system('livestreamer --player=%s %s %s' % (player, url, quality))

        #self.queue.spawn(self.sync_trakt_movies, resp.movies())

    #***************************************************************************

    @Reactor.wamp.register_method(u'media.video.live.play')
    def play(self, url):
        os.system('vlc %s' % url)

        #self.queue.spawn(self.sync_trakt_movies, resp.movies())


from reactor.webapp.shortcuts import *

#*******************************************************************************

from bulbs       import property         as Neo4Prop
from bulbs.utils import current_datetime as Neo4Now

################################################################################

@Reactor.graph.register_node('movie')
class CinemaMovie(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

#*******************************************************************************

@Reactor.graph.register_node('documentary')
class DocumentaryMovie(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

################################################################################

@Reactor.graph.register_node('tv-serie')
class TV_Serie(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

#*******************************************************************************

@Reactor.graph.register_node('tv-show')
class TV_Show(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

################################################################################

@Reactor.graph.register_node('anime-movie')
class AnimeMovie(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

#*******************************************************************************

@Reactor.graph.register_node('anime-serie')
class AnimeSerie(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

################################################################################

@Reactor.graph.register_edge('watch')
class Wacthed(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)

    specs     = Neo4Prop.Dictionary(default={}, nullable=False)

#*******************************************************************************

@Reactor.graph.register_edge('favorite')
class Favorited(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)

    config    = Neo4Prop.Dictionary(default={}, nullable=False)

#*******************************************************************************

@Reactor.graph.register_edge('favorite')
class Favorited(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)

    config    = Neo4Prop.Dictionary(default={}, nullable=False)


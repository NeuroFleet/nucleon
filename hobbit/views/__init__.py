from nucleon.hobbit.shortcuts import *

################################################################################

@login_required
@render_to('hobbit/home.html')
def homepage(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        return redirect('done')

#*******************************************************************************

@render_to('hobbit/crawler.html')
def do_crawler(request):
    resp = {
        'stats': [
    dict(color='blue',   icon='image',   label="Domains", value=0, total=250),
    dict(color='red',    icon='cubes',   label="Spiders", value=0, total=50),
    dict(color='green',  icon='disk',    label="URLs",    value=0, total=5000),
    dict(color='purple', icon='sitemap', label="Runs",    value=0, total=15000),
        ],
        'frm_crawl': CrawlerForm()
    }

    if request.method=='POST':
        resp['frm_crawl'] = CrawlerForm(request.POST)

    return resp

################################################################################

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^crawler$',                       do_crawler, name='crawler'),

    url(r'^$',                              homepage),
]


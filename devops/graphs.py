from reactor.webapp.shortcuts import *

#*******************************************************************************

from bulbs       import property         as Neo4Prop
from bulbs.utils import current_datetime as Neo4Now

################################################################################

@Reactor.graph.register_node('platform')
class CloudPlatform(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

#*******************************************************************************

@Reactor.graph.register_node('team')
class Team(Reactor.graph.Node):
    network   = Neo4Prop.String(nullable=False)
    uid       = Neo4Prop.Long(nullable=False)

    fullname  = Neo4Prop.String()
    age       = Neo4Prop.Integer()
    location  = Neo4Prop.String()

################################################################################

@Reactor.graph.register_edge('belong-to')
class RunsOn(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)

    specs     = Neo4Prop.Dictionary(default={}, nullable=False)

#*******************************************************************************

@Reactor.graph.register_edge('belong-to')
class BelongTo(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)

    config    = Neo4Prop.Dictionary(default={}, nullable=False)


from nucleon.devops.shortcuts import *

################################################################################

PROGRAMMING_HOSTERs = (
    ('bitbucket', "Atlassian BitBucket"),
    ('github',    "GitHub"),
    ('gitlab',    "GitLab community"),
    ('lpad',      "Canonical LaunchPad"),
    ('sfnet',     "SourceForge community"),
)

PROGRAMMING_LANGAUGEs = (
    ('misc',   "Un-identified"),
    ('clang',  "C / C++"),

    ('php',    "PHP"),

    ('python', "Python"),
    ('ruby',   "Ruby"),
)

PROGRAMMING_VERIONNERs = (
    ('bzr', "Bazaar"),
    ('git', "Git"),
    ('hg',  "Mercurial"),
    ('svn', "Subversion"),
)

#*******************************************************************************

@Reactor.orm.register_model('scm_repo')
class ScmRepository(models.Model):
    identity  = models.ForeignKey(Identity, related_name='scm_repos')
    provider  = models.CharField(max_length=64, default='github', choices=PROGRAMMING_HOSTERs)

    owner     = models.CharField(max_length=64)
    alias     = models.CharField(max_length=64)

    summary   = models.TextField(blank=True)
    readme    = models.TextField(blank=True)

    language  = models.CharField(max_length=64, default='misc', choices=PROGRAMMING_LANGAUGEs)
    scm_type  = models.CharField(max_length=64, default='git', choices=PROGRAMMING_VERIONNERs)

    is_public = models.BooleanField(default=True)
    fork_of   = models.ForeignKey('ScmRepository', related_name='forks', blank=True, null=True, default=None)

################################################################################

#class WebProfile(models.Model):
#    pass


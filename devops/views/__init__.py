from nucleon.devops.shortcuts import *

#*******************************************************************************

@login_required
@render_to('devops/home.html')
def homepage(request):
    #django_rq.enqueue(func, foo, bar=baz)

    #import docker

    #prx = docker.Client()

    return {
        'stats': [
    dict(color='blue',   icon='image',   label="Docker Images",     value=0, total=150),
    dict(color='red',    icon='cubes',   label="Docker Containers", value=0, total=50),
    dict(color='green',  icon='disk',    label="Docker Volumes",    value=0, total=100),
    dict(color='purple', icon='sitemap', label="Docker Networks",   value=0, total=20),
        ],
    }

################################################################################

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    #url(r'^search/(?P<facet>[^/]+)$',       Search.perform,    name='search'),

    url(r'^$',                              homepage),
]


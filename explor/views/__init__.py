from nucleon.explor.shortcuts import *

#*******************************************************************************

@login_required
@render_to('explor/home.html')
def homepage(request):
    resp = {
        'stats': [
    dict(color='red',    icon='image',   label="Endpoints",   value=0, total=150),
    dict(color='green',  icon='cubes',   label="FS mounts",   value=0, total=50),
    dict(color='blue',   icon='disk',    label="Directories", value=0, total=100),
    dict(color='purple', icon='sitemap', label="Files",       value=0, total=20),
        ],
    }

    return resp

################################################################################

#from . import Status

################################################################################

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    #url(r'^search/(?P<facet>[^/]+)$',       Search.perform,    name='search'),

    url(r'^$',                              homepage),
]

